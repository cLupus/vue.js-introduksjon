# Vue.js - en introduksjon

En inføring i [Vue.js][] for Bouvet One 2019.

Skrevet med [Vue.js][] og [eagle.js][].

Selve presentasjonen kan sees lokal (følg instruksjonene under), eller [her][]


### Kom i gang

Om du ikke allerede har installert [Node.js][], må det gjøres først.

Last ned, eller klon repoet med:
``` bash
git clone https://gitlab.com/cLupus/vue.js-introduksjon.git
```

Installer alle avhengigeter:
```bash
cd vue.js-introduksjon
npm install
```

Kjør så ```npm run dev``` for å starte en lokal server.
Åpne så nettleren og gå til [http://localhost:8080](http://localhost:8080) for å se presentasjonen.


[Vue.js]: https://vuejs.org/
[eagle.js]: https://github.com/Zulko/eagle.js
[Node.js]: https://nodejs.org/en/

[her]: 
