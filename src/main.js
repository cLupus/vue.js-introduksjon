import Vue from 'vue'

import Eagle, {
  Options,
  CodeBlock,
  CodeComment,
  Presenter
} from 'eagle.js'
// import eagle.js default styles
import 'eagle.js/dist/eagle.css'
import 'eagle.js/dist/themes/agrume/agrume.css'
// import animate.css for slide transition
import 'animate.css'
import App from './App'

import hljs from 'highlight.js'
import javascript from 'highlight.js/lib/languages/javascript'
hljs.registerLanguage('javascript', javascript)

Vue.use(Eagle)

Options.hljs = hljs
Eagle.use(CodeBlock)
Eagle.use(CodeComment)
Eagle.use(Presenter)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  render: h => h(App)
}).$mount('#app')
