module.exports = {
  lintOnSave: 'error',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].template = './src/index.html'
        return args
      })
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? 'vue.js-introduksjon'
    : '/'
  // runtimeCompiler: process.env.NODE_ENV !== 'production'
}
